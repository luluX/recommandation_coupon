# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jul 18 12:31:26 2019

@author: Lucienne
"""
import os
from sklearn.model_selection import train_test_split
from sklearn.ensemble import (GradientBoostingClassifier, RandomTreesEmbedding, 
                              RandomForestClassifier)
from sklearn.preprocessing import OneHotEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline
from sklearn.metrics import roc_curve
import matplotlib.pyplot as plt
import pandas as pd
import time
from sklearn import preprocessing
#from sklearn.preprocessing import LabelEncoder
os.chdir("../input/")
data = pd.read_csv('data_ver23.csv', sep = ',', encoding = 'UTF-8')
#train = pd.read_csv('train.csv', sep = ';', encoding = 'UTF-8')
#test = pd.read_csv('test.csv', sep = ';', encoding = 'UTF-8')
data.dropna(axis=0, how='any', inplace=True)

#def f_dev(x):
#    if str(x) == 'Mobile':
#        x = 1
#        return x
#    elif x == 'Tablet':
#        x = 2
#        return x
#    elif x == 'Desktop':
#        x = 3
#        return x
#    
#def f_countrycode(x):
#    if str(x) == 'a':
#        x = 1
#        return x
#    elif x == 'b':
#        x = 2
#        return x
#    elif x == 'c':
#        x = 3
#        return x
#    elif x == 'd':
#        x = 4
#        return x
#    elif x == 'e':
#        x = 5
#        return x
#    elif x == 'f':
#        x = 6
#        return x
#    
#    
#def f_browser(x):
#    if x == 'Chrome' or x == 'Google Chrome':
#        x = 1
#        return x
#    elif x == 'Edge':
#        x = 2
#        return x
#    elif x == 'Firefox' or x == 'Mozilla Firefox':
#        x = 3
#        return x
#    elif x == 'InternetExplorer' or x == 'Internet Explorer':
#        x = 4
#        return x
#    elif x == 'Mozilla':
#        x = 5
#        return x
#    elif x == 'Opera':
#        x = 6
#        return x
#    elif x == 'IE':
#        x = 7
#        return x
#    elif x == 'Safari':
#        x = 8
#        return x
    

def label (x) :
    le = preprocessing.LabelEncoder()
    le.fit(x)
    return le.transform(x)
    
     


    
#data['devid'] = data['devid'].apply(f_dev)  
data['ind_empleado'] = label(data['ind_empleado'])
data['pais_residencia'] = label(data['pais_residencia'])
data['sexo'] = label(data['sexo'])
data['tiprel_1mes'] = label(data['tiprel_1mes'])
data['indext'] = label(data['indext'])
#data['conyuemp'] = label(data['conyuemp'])
data['nomprov'] = label(data['nomprov'])
data['segmento'] = label(data['segmento'])
data['indresi'] = label(data['indresi'])
data['indfall'] = label(data['indfall'])
data['fecha_dato'] = data['fecha_dato'].apply(lambda x : time.mktime(time.strptime(x, '%Y/%m/%d')))
data['fecha_alta'] = data['fecha_alta'].apply(lambda x : time.mktime(time.strptime(x, '%Y/%m/%d')))
#data['ult_fec_cli_1t'] = data['ult_fec_cli_1t'].apply(lambda x : time.mktime(time.strptime(x, '%Y/%m/%d')))


#train = data[:700000]
#test = data[700001:]
train, test = train_test_split(data,test_size=0.3)

""" base train """
y_train = train['add_not']
train = train.drop('add_not', axis=1)
X_train = train.drop('ncodpers', axis=1)


""" base test """
y_test = test['add_not']
test = test.drop('add_not', axis=1)
X_test = test.drop('ncodpers', axis=1)

X_tree,X_lr,y_tree,y_lr= train_test_split(X_train,y_train,test_size=0.5)


n_estimator = 100

""" Unsupervised transformation based on totally random trees """
rt = rt = RandomTreesEmbedding(max_depth=3, n_estimators=n_estimator,
                          random_state=0)
rt_lm = LogisticRegression(solver='lbfgs', max_iter=1000)
pipeline = make_pipeline(rt, rt_lm)
pipeline.fit(X_tree, y_tree)
y_pred_rt = pipeline.predict_proba(X_test)[:, 1]
fpr_rt_lm, tpr_rt_lm, _ = roc_curve(y_test, y_pred_rt)


""" Supervised transformation based on random forests """
rf = RandomForestClassifier(max_depth=3, n_estimators=n_estimator)
rf_enc = OneHotEncoder()
rf_lm = LogisticRegression(solver='lbfgs', max_iter=1000)
rf.fit(X_tree, y_tree)
rf_enc.fit(rf.apply(X_tree))
rf_lm.fit(rf_enc.transform(rf.apply(X_lr)), y_lr)

y_pred_rf_lm = rf_lm.predict_proba(rf_enc.transform(rf.apply(X_test)))[:, 1]
fpr_rf_lm, tpr_rf_lm, _ = roc_curve(y_test, y_pred_rf_lm)


""" Supervised transformation based on gradient boosted trees """
grd = GradientBoostingClassifier(n_estimators=n_estimator)
grd_enc = OneHotEncoder()
grd_lm = LogisticRegression(solver='lbfgs', max_iter=1000)
grd.fit(X_tree, y_tree)
grd_enc.fit(grd.apply(X_tree)[:, :, 0])
grd_lm.fit(grd_enc.transform(grd.apply(X_lr)[:, :, 0]), y_lr)

y_pred_grd_lm = grd_lm.predict_proba(
    grd_enc.transform(grd.apply(X_test)[:, :, 0]))[:, 1]
fpr_grd_lm, tpr_grd_lm, _ = roc_curve(y_test, y_pred_grd_lm)


""" The gradient boosted model by itself """
y_pred_grd = grd.predict_proba(X_test)[:, 1]
fpr_grd, tpr_grd, _ = roc_curve(y_test, y_pred_grd)

""" The random forest model by itself """
y_pred_rf = rf.predict_proba(X_test)[:, 1]
fpr_rf, tpr_rf, _ = roc_curve(y_test, y_pred_rf)


plt.figure(1)
plt.plot([0, 1], [0, 1], 'k--')
#plt.plot(fpr_rt_lm, tpr_rt_lm, label='RT + LR')
plt.plot(fpr_rf, tpr_rf, label='RF')
#plt.plot(fpr_rf_lm, tpr_rf_lm, label='RF + LR')
plt.plot(fpr_grd, tpr_grd, label='GBDT')
plt.plot(fpr_grd_lm, tpr_grd_lm, label='GBDT + LR')
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curve')
plt.legend(loc='best')
plt.show()

plt.figure(2)
plt.xlim(0.2, 0.4)
plt.ylim(0.6, 0.8)
plt.plot([0, 1], [0, 1], 'k--')
#plt.plot(fpr_rt_lm, tpr_rt_lm, label='RT + LR')
plt.plot(fpr_rf, tpr_rf, label='RF')
#plt.plot(fpr_rf_lm, tpr_rf_lm, label='RF + LR')
plt.plot(fpr_grd, tpr_grd, label='GBDT')
plt.plot(fpr_grd_lm, tpr_grd_lm, label='GBDT + LR')
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curve (zoomed in at top left)')
plt.legend(loc='best')
plt.show()

#gbdt = GradientBoostingClassifier()
#
#gbdt.fit(X_gbdt,y_gbdt)
#
#leaves = gbdt.apply(X_lr)[:,:,0]
#
#ohe = OneHotEncoder()
#
#features_trans = ohe.fit_transform(leaves)
#
#lr= LogisticRegression()
#
#lr.fit(features_trans,y_lr)
#
#lr.predict(features_trans)
#
#lr.predict_proba(features_trans)[:,1]
#
#fpr_grd_lm, tpr_grd_lm, _ = roc_curve(y_test, y_pred_grd_lm)